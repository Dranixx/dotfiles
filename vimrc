"  '##::::'##:'####:'##::::'##:'########:::'######::
"   ##:::: ##:. ##:: ###::'###: ##.... ##:'##... ##:
"   ##:::: ##:: ##:: ####'####: ##:::: ##: ##:::..::
"   ##:::: ##:: ##:: ## ### ##: ########:: ##:::::::
"  . ##:: ##::: ##:: ##. #: ##: ##.. ##::: ##:::::::
"  :. ## ##:::: ##:: ##:.:: ##: ##::. ##:: ##::: ##:
"  ::. ###::::'####: ##:::: ##: ##:::. ##:. ######::
"  :::...:::::....::..:::::..::..:::::..:::......:::

"      ############# vim config ############### 
"      #                                      #
"      #      Configuration of my vim         #
"      #      Autor: Zhan Alexandre           #
"      #      Alias: Dranix                   #
"      #      Email: azhan1996@gmail.com      #
"      #                                      #
"      ############# vim config ###############

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugin with VimPlug
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('~/.vim/plugged')

Plug 'vim-airline/vim-airline'                 " Awesome statusline
Plug 'vim-airline/vim-airline-themes'          " Theme for airline
Plug 'flazz/vim-colorschemes'                  " All colorscheme
Plug 'tpope/vim-fugitive'                      " Git wrapper

Plug 'shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }  " Autocompetion
Plug 'tweekmonster/deoplete-clang2'            " completion C/C++
Plug 'Shougo/neco-vim'                         " completion vim script
Plug 'zchee/deoplete-jedi'                     " completion python

Plug 'scrooloose/nerdtree'                     " Tree on the right
Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/syntastic'                    " Autocorrection
Plug 'raimondi/delimitmate'                    " Auto pairs
Plug 'lilydjwg/colorizer'                      " Color hex in vim
Plug 'ervandew/supertab'                       " Tab for completion 

" Initialize plugin system
call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => .vimrc principal setting
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set encoding=utf-8
set nocompatible                        " Disable compatibility with Vi
filetype on
filetype indent on
filetype plugin on
set history=500                         " Sets how many lines of history VIM has to remember

set list listchars=tab:❯.,trail:.,extends:#,nbsp:★
colorscheme molokai
set number                              " Display line number
set relativenumber
syntax on                               " Syntax color
set showmatch                           " Show matching parenthesis
set mat=2                               " How many tenths of a second to blink when matching bracket

" Indentation setting
set smartindent                         " Smart indent
set autoindent                          " Conserve indent
set shiftwidth=2                        " Use 4 spaces for tb
set tabstop=2
set expandtab                           " Use space instead of tab
autocmd FileType make set noexpandtab   " Not use space in a Makefile
set nowrap                              " No return line if write after the screen

set mouse=a                             " Mouse enable
set showcmd                             " Show partial command in the last line of screen
set ruler                               " Current position of cursor
set wildmenu                            " Visual autocomplete for comand menu

set colorcolumn=80                      " Highlight in red after 80 column
"set cursorline                          " Highlight line
set cursorcolumn                        " Highlight column

set ignorecase                          " Ignore case when searching
set smartcase                           " When searching try to be smart about cases
set hlsearch                            " Highlight search results
set incsearch                           " Makes search act like search in modern browsers

" Allow backspace over everything in insert mode
set backspace=indent,eol,start

" Start vim in where you left last time
if has("autocmd")
        au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
          \| exe "normal! g'\"" | endif
endif
" au FileType * setlocal formatoptions-=cro "Disabled autocomment

set hidden
set pastetoggle=<F2>            " In insert mode <F2> to paste mode
set laststatus=2                " Always display status line

" disable stupid backup and swap files - they trigger too many events for file system watchers
set nobackup
set nowritebackup
set noswapfile

" Set to auto read when a file is changed from the outside
set autoread

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim Mapping
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" save in sudo
command W w !sudo tee % > /dev/null
"save with double Esc
map <Esc><Esc> :w<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Airline customization
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:airline_powerline_fonts = 1
let g:airline_theme='simple'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Deocomplete plugin
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use deoplete.
let g:deoplete#enable_at_startup = 1

" Don't show a preview for python completion
let g:deoplete#sources#jedi#show_docstring = 0

" Don't show preview
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => NERD Tree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Open automatically NERDTree with cursor in the file
autocmd vimenter * NERDTreeFind
autocmd VimEnter * wincmd p

" Map Ctrl-n for opening NERDTree
map <C-n> :NERDTreeToggle<CR>

" Close vim even if NERDTree is open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endifi
" Slpit with s in the right
set splitright
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Syntastic
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => SuperTab
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:SuperTabDefaultCompletionType = "<s-TAB>"

" => Auto pair


